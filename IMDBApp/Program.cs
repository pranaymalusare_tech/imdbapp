﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDBApp;
using IMDBApp.Domain;
using IMDBApp.Repository;

namespace Library
{
    class Program
    {

        static void Main(string[] args)
        {
            IMDBManager imdbmanager = new IMDBManager();
            Console.WriteLine("1. List Movies");
            Console.WriteLine("2. Add Movie");
            Console.WriteLine("3. Add Actor");
            Console.WriteLine("4. Add Producer");
            Console.WriteLine("5. Delete Movie");
            Console.WriteLine("6. Exit");

            string line;

            do
            {
                Console.WriteLine("What Do you want ?");
                line = Console.ReadLine();
                int a = !string.IsNullOrEmpty(line) ? Convert.ToInt32(line) : 0;
                switch (a)
                {
                    case 1:
                        if (imdbmanager.movieRepository.Count() == 0)
                        {
                            Console.WriteLine("No Movie Available in the App.");
                        }
                        else
                        {
                            foreach (Movie movie in imdbmanager.movieRepository.GetAllMovies())
                            {
                                Console.WriteLine("Movie Name - " + movie.MovieName);
                                Console.WriteLine("Plot - " + movie.Plot);
                                var strActor = "";
                                foreach (Person actor in movie.ActorList)
                                {
                                    strActor += actor.Name + ", ";
                                }
                                Console.WriteLine("Actors - " + strActor);
                                Console.WriteLine("Producer - " + movie.Producer.Name);
                            }
                        }
                        break;

                    case 2:
                        if (imdbmanager.actorRepository.Count()  == 0)
                        {
                            Console.WriteLine("Please add actors.");
                        }
                        else if (imdbmanager.producerRepository.Count() == 0)
                        {
                            Console.WriteLine("Please add Producer.");
                        }
                        else
                        {


                            Console.Write("Name:");
                            string movieName = Console.ReadLine();
                            Console.Write("Year:");
                            string stryear = Console.ReadLine();
                            int year = Convert.ToInt32(stryear);
                            Console.Write("Plot:");
                            string plot = Console.ReadLine();
                            Console.Write("Choose Actor(s): ");
                            var actorlist = imdbmanager.actorRepository.GetAllActors();
                            for (int i = 0; i < imdbmanager.actorRepository.Count(); i++)
                            {
                                Console.Write((i + 1) + ". ");
                                Console.Write(actorlist[i].Name + " ");

                            }
                            Console.WriteLine();
                            var actorOptions = Console.ReadLine();
                            var arrayActorIndex = actorOptions.Split(" ").Select(Int32.Parse).ToArray();
                            var allActors = imdbmanager.GetAllActors();
                            List<Person> selectedActors = new List<Person>();
                            for (int i = 0; i < arrayActorIndex.Length; i++)
                            {
                                selectedActors.Add(actorlist.ElementAt(arrayActorIndex[i] - 1));
                            }
                            Console.Write("Choose Producer: ");
                            var producer = imdbmanager.producerRepository.GetAllProducers();
                            for (int i = 0; i < imdbmanager.producerRepository.Count(); i++)
                            {
                                Console.Write((i + 1) + ". ");
                                Console.Write(producer[i].Name + " ");

                            }
                            Console.WriteLine();
                            var producerOption = Console.ReadLine();
                            var prodIndex = Convert.ToInt32(producerOption);
                            var Producer = producer.ElementAt(prodIndex - 1);
                            var movieRepository = new MovieRepository();
                            imdbmanager.AddMovie(movieName, selectedActors, Producer , plot, year);
                        }
                        break;

                    case 3:
                        Console.Write("Name :");
                        var actorName = Console.ReadLine();
                        Console.Write("D.O.B (dd/mm/yyyy) :");
                        var strDate = Console.ReadLine().Split("/").ToArray();
                        int date = Convert.ToInt32(strDate[2]);
                        imdbmanager.AddActor(actorName, date);
                        break;

                    case 4:
                        Console.Write("Name :");
                        var producerName = Console.ReadLine();
                        Console.Write("D.O.B. (dd/mm/yyyy) :");
                        var striDate = Console.ReadLine().Split("/").ToArray();
                        int date_ = Convert.ToInt32(striDate[2]);
                        imdbmanager.AddProducer(producerName,date_);
                        break;

                    case 5:
                        Console.Write("Enter a Name :");
                        var removeName = Console.ReadLine();
                        imdbmanager.RemoveMovie(removeName);

                        break;

                    case 6:
                        return;


                }
            } while (!String.IsNullOrEmpty(line));
        }
    }
}

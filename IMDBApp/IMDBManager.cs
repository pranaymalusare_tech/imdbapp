﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using IMDBApp.Domain;
using IMDBApp.Repository;

namespace IMDBApp
{
    public class IMDBManager
    {
        public ActorRepository actorRepository = new ActorRepository();
        public ProducerRepository producerRepository = new ProducerRepository();
        public MovieRepository movieRepository = new MovieRepository();
        
        public void AddActor(string name , int birthdate)
        {
            var actor = new Person() { Name = name, Birthdate = birthdate };
            actorRepository.AddActor(actor);
        }
        
        public void AddProducer(string name, int birthdate)
        {
            var producer = new Person() { Name = name, Birthdate = birthdate };
            producerRepository.AddProducer(producer);
        }

        public void AddMovie(string movieName, List<Person> actorList, Person producer, string plot, int date)
        {
            
            var objMovie = new Movie
            {
                MovieName = movieName,
                Plot = plot,
                Year = date,
                ActorList = actorList,
                Producer = producer
            };
            movieRepository.AddMovie(objMovie);

            ;

        }

        public void RemoveMovie(string name)
        {
            movieRepository.RemoveMovie(name);
        }

        public List<Movie> GetAllMovies()
        {
            return movieRepository.GetAllMovies();
        }
        
        public List<Person> GetAllActors()
        {
            return actorRepository.GetAllActors();
        }

        public List<Person> GetAllProducers()
        {
            return producerRepository.GetAllProducers();
        }
    }
    
}

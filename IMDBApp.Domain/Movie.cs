﻿using System;
using System.Collections.Generic;

namespace IMDBApp.Domain
{
    public class Movie
    {
        public string MovieName { get; set; }
        public List<Person> ActorList = new List<Person>();
        public Person Producer ;
        public string Plot { get; set; }
        public int Year { get; set; }
    }
}

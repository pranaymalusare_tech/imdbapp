﻿using System;

namespace IMDBApp.Domain
{
    public class Person
    {
        public string Name { get; set; }
        public int Birthdate { get; set; }
    }
}

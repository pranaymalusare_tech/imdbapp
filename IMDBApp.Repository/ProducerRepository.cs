﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDBApp.Domain;

namespace IMDBApp.Repository
{
    public class ProducerRepository
    {
        private List<Person> _producerList = new List<Person>();

        public void AddProducer(Person objProducer)
        {
            _producerList.Add(objProducer); 
        }

        public Person GetAProducer(string name)
        {
            return _producerList.Find(i => i.Name == name);
        }

        public void RemoveProducer(string bookName)
        {
            var _book = GetAProducer(bookName);
            _producerList.Remove(_book);
        }

        public List<Person> GetAllProducers()
        {
            return _producerList;
        }

        public int Count()
        {
            return _producerList.Count;
        }
    }
}

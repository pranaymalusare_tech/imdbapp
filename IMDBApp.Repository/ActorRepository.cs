﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using IMDBApp.Domain;

namespace IMDBApp.Repository
{
    public class ActorRepository
    {
        public List<Person> _actorList = new List<Person>();

        public void AddActor(Person objActor)
        {
            
            _actorList.Add(objActor);

        }
        public Person GetAActor(string name)
        {
            return _actorList.Find(i => i.Name == name);
        }

        public void RemoveActor(string actorname)
        {
            var _actor = GetAActor(actorname);
            _actorList.Remove(_actor);
        }

        public List<Person> GetAllActors()
        {
            return _actorList;
        }

        public int Count()
        {
            return _actorList.Count;
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDBApp.Domain;

namespace IMDBApp.Repository
{
    public class MovieRepository
    {
        private List<Movie> _movieList = new List<Movie>();

        public void AddMovie(Movie objMovie)
        {
            _movieList.Add(objMovie);
        }
        
        public void RemoveMovie(String name)
        {
            var movie = _movieList.Find(i => i.MovieName == name);
            if (movie != null)
            {
                _movieList.Remove(movie);
            }
        }

        public List<Movie> GetAllMovies()
        {
            return _movieList;
        }

        public int Count()
        {
            return _movieList.Count;
        }
    }
}

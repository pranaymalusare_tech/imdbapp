﻿Feature: SpecFlowFeature1
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@Adding
Scenario: Adding a movie in IMDB App
	Given name of the movie is "Harry Potter"
	And actors selected are "1 2"
	And producer selected is "1"
	And plot of the movie is "Plot Plot Plot"
	And the movie released in year "1998"
	When movie is added 
	Then the storage space should look like this 
	| Movie Name   | Plot           | Year |
	| Harry Potter | Plot Plot Plot | 1998 |
	And the list should look like this
	| Name             | Birthdate |
	| Harry Potter     | 1998      |
	| Hermione Granger | 1998      |
	And the list should look lik this
	| Name   | Birthdate |
	| Pranay | 1998      |


@listofmovies
Scenario: displaying list
	Given i have movies in my Imdb App
	When i display my list of movies
	Then the storagelist should look like this 
	| Movie Name   | Plot           | Year |
	| Harry Potter | Plot Plot Plot | 1998 |
	And the list should look like this
	| Name         | Birthdate |
	| Harry Potter | 1998      |
	And the list should look lik this
	| Name   | Birthdate |
	| Pranay | 1998      |
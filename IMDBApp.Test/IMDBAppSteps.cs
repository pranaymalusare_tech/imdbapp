﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDBApp.Domain;
using IMDBApp.Repository;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace IMDBApp.Test
{
    [Binding]
    public class IMDBAppSteps
    {
        IMDBManager iMDBManager = new IMDBManager();
        string MovieName;
        string Plot;
        int MovieYear;
        List<Person> ActorList = new List<Person>();
        Person Producer = new Person();
        List<Movie> newList = new List<Movie>();
        List<Person> actorlist = new List<Person>();
        List<Person> listOfActors = new List<Person>();
        Person producerList = new Person();


        [Given(@"name of the movie is ""(.*)""")]
        public void GivenNameOfTheMovieIs(string movieName)
        {
            MovieName=movieName;
        }

        [Given(@"actors selected are ""(.*)""")]
        public void GivenActorsSelectedAre(string actorIndex)
        {
            var ActorIndex = actorIndex.Split(" ").Select(Int32.Parse).ToArray();
            var actorlist = iMDBManager.GetAllActors();
            for (int i = 0; i < ActorIndex.Length; i++)
            {
                ActorList.Add(actorlist.ElementAt(ActorIndex[i] - 1));
            }
            //ScenarioContext.Current.Pending();
        }

        [Given(@"producer selected is ""(.*)""")]
        public void GivenProducerSelectedIs(int producerIndex)
        {
            var producerList = iMDBManager.GetAllProducers();
            Producer = producerList.ElementAt(producerIndex - 1);
            //ScenarioContext.Current.Pending();
        }

        [Given(@"plot of the movie is ""(.*)""")]
        public void GivenPlotOfTheMovieIs(string plot)
        {
            Plot = plot; 
        }
        
        [Given(@"the movie released in year ""(.*)""")]
        public void GivenTheMovieReleasedInYear(int movieYear)
        {
            MovieYear = movieYear;

        }

        [Given(@"i have a movie in List where movie name is ""(.*)""")]
        public void GivenIHaveAMovieInListWhereMovieNameIs(string movieName)
        {
            MovieName = movieName;
        }

        [Given(@"i have movies in my Imdb App")]
        public void GivenIHaveMoviesInMyImdbApp()
        {
           
           // ScenarioContext.Current.Pending();
        }
        
        [When(@"movie is added")]
        public void WhenMovieIsAdded()
        {
            iMDBManager.AddMovie(MovieName, ActorList , Producer, Plot, MovieYear);
            newList = iMDBManager.GetAllMovies();
        }

        [When(@"i check my actor list")]
        public void WhenICheckMyActorList()
        {
            newList = iMDBManager.GetAllMovies();
            Movie movie = newList.Find(i => i.MovieName == MovieName);
            actorlist = movie.ActorList;

        }

        [When(@"i check Producer Name")]

        public void WhenICheckProducerName()
        {
            newList = iMDBManager.GetAllMovies();
            Movie movie = newList.Find(i => i.MovieName == MovieName);
            producerList = movie.Producer;
        }


        
        [When(@"i display my list of movies")]
        public void WhenIDisplayMyListOfMovies()
        {
            newList = iMDBManager.GetAllMovies();
        }
        
        [Then(@"the storage space should look like this")]
        public void ThenTheStorageSpaceShouldLookLikeThis(Table table)
        {
            table.CompareToSet(newList);
        }
        
        [Then(@"the list should look like this")]

        public void ThenTheListShouldLookLikeThis(Table table)
        {
            var ActorsList = new List<Person>();
            foreach (var mov in newList)
                foreach (var actr in mov.ActorList)
                    ActorsList.Add(actr);
            table.CompareToSet(ActorsList);
        }

        [Then(@"the list should look lik this")]

        public void ThenTheListShouldLookLikThis(Table table)
        {
            var ProducerList = new List<Person>();
            foreach (var mov in newList)
                ProducerList.Add(mov.Producer);
            table.CompareToSet(ProducerList);
        }

        [Then(@"the storagelist should look like this")]
        public void ThenTheStoragelistShouldLookLikeThis(Table table)
        {
            table.CompareToSet(newList);
        }

        [BeforeScenario("Adding")]

        public void AddProdAndActor()
        {
            iMDBManager.AddActor("Harry Potter",1998);
            iMDBManager.AddActor("Hermione Granger", 1998);
            iMDBManager.AddProducer("Pranay",1998);
        }

        [BeforeScenario("listofmovies")]
        public void AddMovie()
        {
            Producer = new Person() { Name = "Pranay", Birthdate = 1998 };
            actorlist.Add(new Person() { Name = "Harry Potter", Birthdate = 1998 });
            //iMDBManager.AddActor("Harry Potter", 1998 );
            //iMDBManager.AddProducer("Pranay", 1998 );
            iMDBManager.AddMovie("Harry Potter", actorlist , Producer , "Plot Plot Plot", 1998);
        }
    }
}
